import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

const FooterMenu = () => {
    return (
        <Footer style={{ textAlign: 'center' }}>AdithyaNuzPratamaPutra</Footer>
    );
}

export default FooterMenu;