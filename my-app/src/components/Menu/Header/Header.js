import React from 'react';
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';

const { Header } = Layout;

const HeaderMenu = () => {
    return (
        <Header style={{ padding: 0 }}>
            <Menu style={{ float: 'right' }} theme="dark" mode="horizontal">
                {/*{user &&
                    (
                        <>
                            <Menu.Item>{user.name}</Menu.Item>
                        </>
                    )

                }*/}
                <Menu.Item key="1"><Link to="/About" >About</Link></Menu.Item>
            </Menu>
        </Header>
    );
}

export default HeaderMenu;